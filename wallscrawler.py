__author__ = 'duongpv'

import logging
import datetime
import os
from time import sleep
from lxml.cssselect import CSSSelector
from lxml.html import *
from sqlalchemy import *

import mechanize
import cookielib

LIMIT = 2
ROOT_DIR = '/home/ubuntu/wp'
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'

logging.getLogger('sqlalchemy.engine').setLevel(logging.WARN)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s [%(levelname)s] %(message)s',
                    filename='crawler.log')
log = logging.getLogger(__name__)

engine = create_engine('mysql://root:123456@localhost:3306/zedge?charset=utf8', echo=False)

wallpapers_urls = Table('wallpapers_urls', MetaData(engine),
                        Column('id', Integer, primary_key=True),
                        Column('url', String),
                        Column('category', String),
                        Column('created', DateTime),
                        Column('updated', DateTime),
                        Column('download', Integer),
                        Column('failed', Integer))

wallpapers = Table('wallpapers', MetaData(engine),
                   Column('id', Integer, primary_key=True),
                   Column('name', String),
                   Column('category', String),
                   Column('path', DateTime),
                   Column('created', DateTime),
                   Column('updated', Integer))


def css_selector(page, selector):
    return list(CSSSelector(selector)(fromstring(page)))


def crawl(id, url, category, downloaded):
    cj = cookielib.LWPCookieJar()

    br = mechanize.Browser()
    br.set_cookiejar(cj)
    # br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)

    br.addheaders = [('User-agent', USER_AGENT)]

    download = 0
    fail = 0

    r = br.open(url)
    page = r.read()
    priviews = css_selector(page, '.image-preview')
    for prv in priviews:
        try:
            dl = prv.attrib['href'].replace('http://www.zedge.net/wallpaper/', 'http://www.zedge.net/item/download/1/')

            imgs = css_selector(tostring(prv), 'img')
            title = imgs[0].attrib['title']
            thumb = imgs[0].attrib['src']

            dir = '%s/%s' % (ROOT_DIR, category.replace(' ', '_').replace('/', '_').replace('&', '_').lower())
            if not os.path.exists(dir):
                os.makedirs(dir)
                log.info('[mkdir] %s' % dir)

            name = title.replace(' ', '_').replace('/', '_').replace('&', '_').lower()
            file = '%s/%s.jpg' % (dir, name)
            if os.path.isfile(file):
                log.info('[exist] %s' % file)
                continue

            dl_page = br.open(dl).read()
            dl_link = 'http://www.zedge.net%s' % css_selector(dl_page, '.downtemp')[0].attrib['href']

            sleep(2)

            br.open(dl_link)
            data = br.response().read()
            size = len(data)
            if size > 50000:
                with open(file, 'wb') as f:
                    f.write(data)

                with open('%s/thumb_%s.jpg' % (dir, name), 'wb') as t:
                    br.open(thumb)
                    t.write(br.response().read())

                download += 1

                wallpapers.insert().execute({
                    'name': title,
                    'category': category,
                    'path': file.replace(ROOT_DIR, ''),
                    'created': datetime.datetime.now(),
                    'updated': datetime.datetime.now()
                })

                log.info('%d %s' % (size, file))

            else:
                fail += 1

        except:
            continue

        sleep(2)

    download += downloaded
    wallpapers_urls.update().values(updated=datetime.datetime.now(),
                                    download=download,
                                    failed=fail).where(wallpapers_urls.c.id == id).execute()

    log.info('updated %s' % url)


rs = wallpapers_urls.select(or_(wallpapers_urls.c.download == 0, wallpapers_urls.c.failed != 0)).order_by(wallpapers_urls.c.id.asc()).limit(LIMIT).execute()
urls = []
for r in rs:
    urls.append({
        'id': r['id'],
        'url': r['url'],
        'category': r['category'],
        'download': r['download']
    })

for url in urls:
    crawl(url['id'], url['url'], url['category'], url['download'])
