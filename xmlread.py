import os
import xml.etree.ElementTree as et
from shutil import copyfile

ROOT_DIR = 'D:/qa'
OUTPUT_DIR = 'D:/qa/result'
SUMMARIES_FILE = 'SUMMARIES_FILE.csv'


def mkdir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


def get_key(item):
    return item['tc']


def get_result(tc):
    src = tc['logpath'].replace('./', '%s/' % ROOT_DIR)
    with open(src, 'r') as f:
        lines = f.readlines()

    data = {}
    for l in lines:
        l = l.strip('\n')
        pid = l[1:l.find('-') - 2]
        log = l[l.rfind('-') + 2:]

        if 'starting' in log or 'started' in log or 'finished' in log:
            continue

        if pid not in data:
            data[pid] = {
                'fail': 0,
                'success': 0,
                'total': 0
            }

        data[pid]['total'] += 1
        if 'passed' in log:
            data[pid]['success'] += 1
        if 'FAILED' in log:
            data[pid]['fail'] += 1

    return data


def process():
    testcases = []

    xmls = next(os.walk('%s/xml_runner' % ROOT_DIR))[2]
    for xml_file in xmls:
        path = '%s/xml_runner/%s' % (ROOT_DIR, xml_file)
        with open(path, 'r') as f:
            xml = f.read()
            tree = et.fromstring(xml)

            for suite in tree.iterfind('TestSuite'):
                for tc in suite.findall('TestCase'):
                    testcases.append({
                        'name': tc.get('name'),
                        'logpath': tc.get('logpath'),
                        'testsuite': suite.get('name'),
                        'xml': xml_file
                    })

            for tc in tree.iterfind('TestCase'):
                testcases.append({
                    'name': tc.get('name'),
                    'logpath': tc.get('logpath'),
                    'testsuite': '',
                    'xml': xml_file
                })

    for tc in testcases:
        rs = get_result(tc)
        tc['rs'] = rs

    for tc in testcases:
        xml_file = tc['xml'].replace('.xml', '')
        for pid in tc['rs'].keys():
            logpath = tc['logpath']
            dir = '%s/%s/%s/%s' % (OUTPUT_DIR, xml_file, tc['name'], pid)
            mkdir(dir)

            src = tc['logpath'].replace('./', '%s/' % ROOT_DIR)
            dst = '%s/%s' % (dir, logpath[logpath.rfind('/') + 1:])

            copyfile(src, dst)
            print '%s -> %s' % (src, dst)

    data = []
    for tc in testcases:
        rs = get_result(tc)
        tc['rs'] = rs
        for key in tc['rs'].keys():
            data.append({
                'pid': key,
                'xml': tc['xml'],
                'tc': tc['name'],
                'ts': tc['testsuite'],
                'total': rs[key]['total'],
                'success': rs[key]['success'],
                'fail': rs[key]['fail'],
            })

    data = sorted(data, key=get_key)

    mkdir(OUTPUT_DIR)
    with open('%s/%s' % (OUTPUT_DIR, SUMMARIES_FILE), 'w') as f:
        f.write('pid,xml_file,test_case,test_suite,success,failure,total\n')
        f.writelines(['%s,%s,%s,%s,%s,%s,%s\n' % (tc['pid'], tc['xml'], tc['tc'], tc['ts'], tc['success'], tc['fail'], tc['total']) for tc in data])


if __name__ == "__main__":
    process()
