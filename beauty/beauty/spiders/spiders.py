from scrapy.spiders import BaseSpider
from scrapy.http import Request
from db_connector import *


def get_detail(response):
    item = response.meta['item']

    if response.css('.product-options'):
        item['option'] = 'true'
    else:
        item['option'] = 'false'

    item['description'] = ''

    if len(response.css('.std')) > 1:
        item['description'] = response.css('.std')[1].extract().replace('\n', '').replace('\r', '')

        etudehouses.insert().execute({
            'name': item['name'],
            'img': item['img'],
            'url': item['url'],
            'old_price': item['old_price'],
            'price': item['price'],
            'availability': item['availability'],
            'option': ', '.join(item['option']),
            'description': item['description']
        })

    print item['name']


class MySpider(BaseSpider):
    name = 'etudehouse_spider'
    allowed_domains = ['http://www.etudehouse.com/']
    start_urls = ['http://www.etudehouse.com/index.php/skincare/toner.html?limit=100',
                  'http://www.etudehouse.com/index.php/skincare/lotion.html?limit=100']

    def parse(self, response):
        products = response.css('.product-item')
        for product in products:
            item = {
                'name': product.css('.product-name')[0].css('::text')[0].extract(),
                'img': product.css('.product-image')[0].css('img').css('::attr(src)').extract_first(),
                'url': product.css('.product-name')[0].css('a')[0].css('::attr(href)')[0].extract()
            }

            if len(product.css('.price-box')[0].css('p')) == 2:
                item['old_price'] = product.css('.price-box')[0].css('p')[0].css('.price').css('::text').extract_first().strip()
                item['price'] = product.css('.price-box')[0].css('p')[1].css('.price').css('::text').extract_first().strip()
            else:
                item['old_price'] = ''
                item['price'] = product.css('.price-box')[0].css('.price').css('::text').extract_first().strip()

            if not product.css('.out-of-stock'):
                item['availability'] = 'In stock'
            else:
                item['availability'] = 'Out of stock'

            request = Request(item['url'], get_detail)
            request.meta['item'] = item
            yield request
