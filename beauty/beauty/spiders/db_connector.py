from sqlalchemy import *

engine = create_engine('mysql://root:123456@localhost:3306/beauty?charset=utf8', echo=False)

beauties = Table('beauty', MetaData(engine),
                 Column('id', Integer, primary_key=True),
                 Column('name', String),
                 Column('old_price', String),
                 Column('price', String),
                 Column('branch', String),
                 Column('capacity', String),
                 Column('weight', String),
                 Column('option', String),
                 Column('img_src', String))

etudehouses = Table('etudehouse', MetaData(engine),
                    Column('id', Integer, primary_key=True),
                    Column('name', String),
                    Column('img', String),
                    Column('url', String),
                    Column('old_price', String),
                    Column('price', String),
                    Column('availability', String),
                    Column('option', String),
                    Column('description', String))
