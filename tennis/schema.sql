DROP TABLE IF EXISTS `stats`;
CREATE TABLE `stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` varchar(32) NOT NULL,
  `player_name` varchar(256) NOT NULL,
  `rank` int(11) NOT NULL,
  `tab` varchar(32) NOT NULL,
  `surface` varchar(32) NOT NULL,
  `stats` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;