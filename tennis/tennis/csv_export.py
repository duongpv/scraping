import csv
import json
from spiders.connector import *

for tab in ['atp', 'wta']:
    for surface in ['', '1', '10', '20']:
        rs = match_stats \
            .select(and_(match_stats.c.surface == surface, match_stats.c.tab == tab)) \
            .order_by(match_stats.c.rank.asc())\
            .limit(20)\
            .execute()

        data = []
        for r in rs:
            d = {
                'playerid': r['player_id'],
                'name': r['player_name'],
                'rank': r['rank']
            }

            d.update(json.loads(r['stats']))
            data.append(d)

        fieldnames = ['rank', 'playerid', 'name']
        for key in data[0].keys():
            if key not in fieldnames:
                fieldnames.append(key)

        with open('match_stats_%s_%s.csv' % (tab, surface), 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for d in data:
                writer.writerow(d)
