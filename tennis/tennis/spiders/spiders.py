import json
from scrapy.http import *
from scrapy.spiders.init import InitSpider
from connector import *

top = 200
tab = 'atp'  # wta
user = 'Axelle2014',
pwd = 'Axelle2014'


def get_stats(response):
    player = response.meta['player']
    surface = response.meta['surface']
    stats = {}

    table = response.css('.match-stats-table')[0]
    for tr in table.css('tr'):
        if len(tr.css('td')) != 3:
            continue

        if tr.css('td')[0].css('b'):
            key = tr.css('td')[1].css('b')[0].css('::text').extract_first()
            val = tr.css('td')[0].css('b')[0].css('::text').extract_first()
        else:
            key = tr.css('td')[1].css('::text').extract_first()
            val = tr.css('td')[0].css('::text').extract_first()

        stats[key.strip()] = val.strip()

    match_stats.insert().execute({
        'player_id': player['id'],
        'player_name': player['name'],
        'rank': player['rank'],
        'tab': tab,
        'surface': surface,
        'stats': json.dumps(stats)
    })

    print '#%s %s' % (player['rank'], player['name'])


def get_player_info(response):
    player = response.meta['player']
    player['name'] = response.css('.profile-title').css('::text').extract_first().strip()
    player['rank'] = int(response.css('.rank').css('span').css('::text').extract_first().strip())

    for surface in ['', '1', '10', '20']:
        request = FormRequest.from_response(response,
                                            url='http://tennisinsight.com/wp-admin/admin-ajax.php',
                                            formdata={'action': 'matchStats',
                                                      'player_id': player['id'],
                                                      'player_id2': '',
                                                      'duration': '380',
                                                      'tour': '1',
                                                      'surface': surface,
                                                      'draw': 'all',
                                                      'criteria': '1',
                                                      'matchID': '',
                                                      'tournamentID': ''},
                                            callback=get_stats)
        request.meta['player'] = player
        request.meta['surface'] = surface

        yield request


class TennisSpider(InitSpider):
    name = 'tennis_spider'
    allowed_domains = ['tennisinsight.com']
    start_urls = [
        'http://tennisinsight.com/wp-admin/admin-ajax.php?action=rankingsTable&type=%s&sEcho=2&iColumns=3&sColumns=&iDisplayStart=0&iDisplayLength=%s&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&iSortCol_0=0&sSortDir_0=asc&iSortingCols=1&bSortable_0=true&bSortable_1=false&bSortable_2=true' % (tab, top)
    ]

    def init_request(self):
        """This function is called before crawling starts"""
        return Request(url='http://tennisinsight.com/wp-login.php', callback=self.login)

    def login(self, response):
        """Generate a login request"""
        return FormRequest.from_response(
            response,
            formdata={'log': user, 'pwd': pwd},
            callback=self.after_login
        )

    def after_login(self, response):
        return self.initialized()

    def parse(self, response):
        for player in json.loads(response.body)['aaData']:
            url = HtmlResponse(url='', body=str(player[2])).css('a')[0].css('::attr(href)').extract_first().strip()

            request = Request(url=url, callback=get_player_info)
            request.meta['player'] = {
                'id': url.split('playerid=')[1]
            }

            yield request
