from sqlalchemy import *

engine = create_engine('mysql://root:123456@localhost:3306/tennis?charset=utf8', echo=False)

match_stats = Table('stats', MetaData(engine),
                    Column('id', Integer, primary_key=True),
                    Column('player_id', String),
                    Column('player_name', String),
                    Column('rank', Integer),
                    Column('tab', String),
                    Column('surface', String),
                    Column('stats', String))
